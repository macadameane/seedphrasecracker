# Seedphrase Cracker #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Seed Cracker is an inefficient set of algorithms that test various combinations of a valid BIP39 wordlist. It substitutes unknown words and then
attemps to use the BSC Scan API to determine if there have been any transactions.  The application attemps to stay within your bscscan rate limits.

### How do I get set up? ###

You will need .NET 6 and Visual Studio to compile the project.  You will also need a BSC Scan API key.  If you already have or obtain a [bscscan.com](https://bscscan.com) account, you can sign up for a [free API key](https://bscscan.com/myapikey).

Once you have a key, follow these steps:
- In appsettings.json:
    - replace the placeholder with your API key
    - if you have different rate limits that the free tier update your limits.
- User Secrets are built into the project.  If you would like to overide the API key config item so it doens't change the code base, right click the project in Visual Studio and select "Manage User Secrets"

### Algorithms

All algorithms inherit from a common base class `Algorithm`.  The base class asks its inheritor to provide the next seedphrase.  It then verifies it
and attempts to check BSC Scan for transaction information.  By default it is looking for BEP20 transactions, but this can be changed `BscScanQuery.Action`

#### Sequential

The sequential algorithm replaces `null` seedwords with one from the wordlist in alphabetical order.  It iterates the word for the last null position first, and when it reaches the last one, it will iterate one word for the next null index found.

#### CycleEach

This algorithm steps through the words one at a time and tries all the words in each position without changing the others. This is good for phrases where a
single word may be incorrect.  Null values are not allowed in this algorithm.

#### Random

The random algorithm simply replaces any null words with a random word from the wordlist.

### Other Notes

Output is only shown for Addresses that contain transactions of the type specified by `BscScanQuery`.  This is highlight any activity found.