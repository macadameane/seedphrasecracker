﻿using SeedphraseCracker.Utilities;

namespace SeedphraseCracker.Algorithm
{
    public class RandomAlgorithm : Algorithm
    {
        private Random random;

        public long RunCount { get; private set; } = 0;


        public RandomAlgorithm(Seedphrase seedphrase, long maxIterations = long.MaxValue) : base(seedphrase)
        {
            random = new Random((int)DateTime.UtcNow.Ticks);
        }

        protected override bool NextSeedphrase()
        {
            for (int i = 0; i < nullIndices.Length; ++i)
            {
                seedphrase[nullIndices[i]] = seedphrase.Wordlist.GetWordAtIndex(random.Next(seedphrase.Wordlist.WordCount));
            }

            ++RunCount;

            return true;
        }
    }
}
