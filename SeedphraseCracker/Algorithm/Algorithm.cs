﻿using Microsoft.Extensions.Configuration;
using SeedphraseCracker.BscScan;
using SeedphraseCracker.Utilities;

namespace SeedphraseCracker.Algorithm
{
    public abstract class Algorithm
    {
        private BscScanApi bscScanApi;
        private BscScanQuery bscScanQuery;
        protected Seedphrase seedphrase;
        protected int[] nullIndices;

        public ulong? MaxIterations { get; private set; } = null;

        public ulong IterationCount { get; private set; } = 0;

        public Algorithm(Seedphrase seedphrase)
        {
            IConfiguration config = Config.Get();

            this.seedphrase = seedphrase;
            nullIndices = seedphrase.GetNullIndices();

            bscScanApi = new BscScanApi(config);
            bscScanQuery = new BscScanQuery(config);
        }

        public async Task Run(ulong? maxIterations = null)
        {
            MaxIterations = maxIterations;
            DateTime time = DateTime.Now;

            while (NextSeedphrase() || IterationCount == MaxIterations)
            {
                ++IterationCount;

                if (seedphrase.IsValidChecksum())
                {
                    WalletInfo wi = seedphrase.GetWalletInfo();
                    bscScanQuery.Address = wi.Address;

                    var result = await bscScanApi.Call(bscScanQuery);

                    if (result["message"]?.GetValue<string>() == "OK")
                    {
                        var txList = result["result"]?.AsArray();
                        if (txList?.Count > 0)
                        {
                            wi.TxCount = txList.Count;
                            Console.WriteLine(wi);
                            Console.WriteLine();
                        }
                    }
                    //else
                    //{
                    //    Console.WriteLine(wi.ToString());
                    //    Console.Write("Error:\n" + result.ToString());
                    //    Console.WriteLine();
                    //}
                }
            }
        }

        protected abstract bool NextSeedphrase();
    }
}
