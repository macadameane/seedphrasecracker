﻿using SeedphraseCracker.Utilities;

namespace SeedphraseCracker.Algorithm
{
    public class CycleEachAlgorithm : Algorithm
    {
        private Seedphrase original;
        private int currentSeedphraseIndex = 1;
        private int currentWordlistIndex = 0;

        public CycleEachAlgorithm(Seedphrase seedphrase) : base(seedphrase)
        {
            original = seedphrase.Copy();

            if (nullIndices.Length != 0)
                throw new ArgumentException("Seedphrase indices cannot be null");
        }

        protected override bool NextSeedphrase()
        {
            seedphrase = original.Copy();

            if (currentWordlistIndex == seedphrase.Wordlist.WordCount)
            {
                if (currentWordlistIndex == seedphrase.WordCount)
                    return false;

                currentWordlistIndex = 0;
                ++currentSeedphraseIndex;
            }

            seedphrase[currentSeedphraseIndex] = seedphrase.Wordlist.GetWordAtIndex(currentWordlistIndex);

            return true;
        }
    }
}
