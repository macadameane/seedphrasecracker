﻿using SeedphraseCracker.Utilities;

namespace SeedphraseCracker.Algorithm
{
    public class SequentialAlgorithm : Algorithm
    {
        private int[] wordListIndices;

        public SequentialAlgorithm(Seedphrase seedphrase) : base(seedphrase)
        {
            wordListIndices = new int[nullIndices.Length];

            for (int i = 0; i < nullIndices.Length; ++i)
            {
                seedphrase[nullIndices[i]] = seedphrase.Wordlist.GetWordAtIndex(wordListIndices[i]);
            }
        }

        protected override bool NextSeedphrase()
        {
            for (int i = wordListIndices.Length - 1; i >= 0; --i)
            {
                if (wordListIndices[i] == seedphrase.Wordlist.WordCount)
                {
                    if (i != 0)
                    {
                        wordListIndices[i] = 0;
                        wordListIndices[i - 1] += 1;
                        seedphrase[nullIndices[i]] = seedphrase.Wordlist.GetWordAtIndex(wordListIndices[i]);
                    }
                }
                else
                {
                    seedphrase[nullIndices[i]] = seedphrase.Wordlist.GetWordAtIndex(wordListIndices[i]);

                    ++wordListIndices[wordListIndices.Length - 1];

                    return true;
                }
            }

            return false;
        }
    }
}
