﻿using SeedphraseCracker.Algorithm;
using SeedphraseCracker.Utilities;

Seedphrase seedphrase = new();
seedphrase[1] = null; //step?
seedphrase[2] = "exercise";
seedphrase[3] = null; //zero?
seedphrase[4] = null; //claim? glass?
seedphrase[5] = "youth";
seedphrase[6] = "cause";
seedphrase[7] = "problem";
seedphrase[8] = null; //woman?
seedphrase[9] = null; //faith?
seedphrase[10] = "visa";
seedphrase[11] = "endorse";
seedphrase[12] = "keep";

//seedphrase[1] = "step";
//seedphrase[3] = "zero";
//seedphrase[4] = "claim";
//seedphrase[8] = "woman";
//seedphrase[9] = "faith";

//Algorithm alg = new SequentialAlgorithm(seedphrase);
//Algorithm alg = new CycleEachAlgorithm(seedphrase);
Algorithm alg = new RandomAlgorithm(seedphrase);

Console.WriteLine("Starting.");
await alg.Run();
Console.WriteLine("Finished.");
