﻿using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using System.Text.RegularExpressions;

namespace SeedphraseCracker.BscScan
{
    public class BscScanQuery
    {
        public string BaseUrl { get; private set; }
        private readonly string ApiKey;

        public string Module { get; set; } = "account";

        private string action = "";
        private BscScanEndpoint endpoint;
        public BscScanEndpoint Action
        {
            get
            {
                return endpoint;
            }
            set
            {
                endpoint = value;
                switch(value)
                {
                    case BscScanEndpoint.TransactionNormal:
                        action = "txlist";
                        break;
                    case BscScanEndpoint.TransactionInternal:
                        action = "txlistinternal";
                        break;
                    case BscScanEndpoint.TransactionBep20:
                        action = "tokentx";
                        break;
                }
            }
        }

        private int offset = 10;
        public int Offset
        {
            get { return offset; }

            set
            {
                if (value <= 0 || value > 10000)
                    throw new ArgumentException("Must be in range 1 to 10000.");

                offset = value;
            }
        }

        private int page = 1;
        public int Page
        {
            get { return page; }

            set
            {
                if (value <= 0)
                    throw new ArgumentException("Must be greater than 1.");

                page = value;
            }
        }

        private readonly Regex addressRegex = new("^0x[0-9a-fA-F]{40}$");
        private string? address;
        public string? Address
        {
            get
            {
                return address;
            }
            set
            {
                if (value == null || !addressRegex.IsMatch(value))
                    throw new ArgumentException("Address must be in the form of a BSC address.");

                address = value;
            }
        }

        public BscScanQuery(IConfiguration config)
        {
            ApiKey = config.GetValue<string>("BscScanApi:Key");
            BaseUrl = config.GetValue<string>("BscScanApi:Url");

            Action = BscScanEndpoint.TransactionBep20;
        }

        public enum BscScanEndpoint
        {
            TransactionNormal,
            TransactionInternal,
            TransactionBep20
        }

        public override string ToString()
        {
            if (address == null)
                throw new InvalidOperationException("Address has not been provided");

            Dictionary<string, string> query = new()
            {
                { "apikey", ApiKey },
                { "module", Module },
                { "action", action },
                { "page", page.ToString() },
                { "offset", offset.ToString() },
                { "address", address }
            };

            var uri = new Uri(QueryHelpers.AddQueryString(BaseUrl, query));
            return uri.ToString();
        }
    }
}
