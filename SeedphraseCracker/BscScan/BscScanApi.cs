﻿using Microsoft.Extensions.Configuration;
using System.Diagnostics;
using System.Text.Json.Nodes;

namespace SeedphraseCracker.BscScan
{
    public class BscScanApi
    {
        private HttpClient httpClient;

        public int RateLimitPerSecond { get; private set; }
        public int RateLimitPerDay { get; private set; }

        private int callsMadeThisSecond;
        private int callsMadeToday;

        private int MillisPerDay = 1000 * 60 * 60 * 24 + 1000;

        private Stopwatch callSetStopWatch;
        private Stopwatch dayStopWatch;

        public BscScanApi(IConfiguration config)
        {
            httpClient = new HttpClient();

            RateLimitPerSecond = config.GetValue<int>("BscScanApi:RateLimitPerSecond");
            RateLimitPerDay = config.GetValue<int>("BscScanApi:RateLimitPerDay");

            callSetStopWatch = new Stopwatch();
            dayStopWatch = new Stopwatch();
        }

        public async Task<JsonObject> Call(BscScanQuery query)
        {
            if (callsMadeToday == RateLimitPerDay)
            {
                if (dayStopWatch.ElapsedMilliseconds < MillisPerDay)
                {
                    await Task.Delay(MillisPerDay - (int)dayStopWatch.ElapsedMilliseconds);
                }

                callsMadeToday = 0;
                dayStopWatch.Restart();
            }
            if (callsMadeThisSecond == RateLimitPerSecond)
            {
                //Adding 300 milliseconds because I still had some rejected requests
                //at 1200ms.  As currently designed, a rejected request would skip the phrase
                //for sequential algorithms.
                if (callSetStopWatch.ElapsedMilliseconds < 1300)
                    await Task.Delay(1300 - (int)callSetStopWatch.ElapsedMilliseconds);

                callsMadeThisSecond = 0;
                callSetStopWatch.Restart();
            }

            HttpResponseMessage response = await httpClient.GetAsync(query.ToString());

            ++callsMadeThisSecond;
            ++callsMadeToday;

            response.EnsureSuccessStatusCode();

            var retVal = JsonObject.Parse(await response.Content.ReadAsStreamAsync())?.AsObject();

            if (retVal == null)
                throw new Exception("Did not receive a JSON response from base url: " + query.BaseUrl);

            return retVal;
        }
    }
}
