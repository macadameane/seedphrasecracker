﻿using Microsoft.Extensions.Configuration;

namespace SeedphraseCracker.Utilities
{
    public class Config
    {
        private static IConfiguration? config;

        public static IConfiguration Get(string? path = null)
        {
            if (config == null)
            {
                if (path == null)
                    path = Environment.CurrentDirectory + "\\appsettings.json";

                if (!File.Exists(path))
                    throw new ArgumentException("Path file doesn't exist");

                config = new ConfigurationBuilder()
                    .AddJsonFile(path, false)
                    .AddUserSecrets<Config>()
                    .Build();
            }

            return config;
        }
    }
}
