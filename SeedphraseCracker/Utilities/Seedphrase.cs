﻿using NBitcoin;
using Nethereum.HdWallet;
using System.Text;

namespace SeedphraseCracker.Utilities
{
	public class Seedphrase
	{
		public Wordlist Wordlist { get; private set; }

		private string?[] seedWords;

		public int WordCount { get { return seedWords.Length; } }

		public Seedphrase() : this(12, Wordlist.English)
		{
		}

		public Seedphrase(int size, Wordlist? wordlist = null)
		{
			CheckWordCount(size);

			seedWords = new string[size];
			Wordlist = wordlist ?? Wordlist.English;
		}

		public Seedphrase(string words, Wordlist? wordlist = null)
		{
			Wordlist = wordlist ?? Wordlist.English;

			seedWords = words.Trim().Split(' ');

			CheckWordCount(seedWords.Length);

			foreach (var word in seedWords)
			{
				CheckValidWord(word);
			}
		}

		public string? this[int key]
		{
			get
			{
				if (key < 1 || key > seedWords.Length)
				{
					throw new IndexOutOfRangeException();
				}

				return seedWords[key - 1];
			}

			set
			{
				if (key < 1 || key > seedWords.Length)
					throw new IndexOutOfRangeException();

				CheckValidWord(value);

				seedWords[key - 1] = value;
			}
		}

		public bool TryAdd(string word, out int index)
		{
			CheckValidWord(word);

			for (int i = 0; i < seedWords.Length; ++i)
			{
				if (seedWords[i] == null)
				{
					seedWords[i] = word;
					index = i + 1;
					return true;
				}
			}

			index = 0;
			return false;
		}

		public int[] GetNullIndices()
		{
			int[] indices = new int[24];
			int indicesIndex = 0;

			for (int i = 0; i < seedWords.Length; ++i)
			{
				if (seedWords[i] == null)
				{
					indices[indicesIndex++] = i + 1;
				}
			}

			Array.Resize<int>(ref indices, indicesIndex);

			return indices;
		}

		public bool IsValidChecksum()
		{
			CheckNullEntries();

			return ToMnemonic().IsValidChecksum;
		}

		public Mnemonic ToMnemonic()
		{
			CheckNullEntries();

			return new Mnemonic(String.Join(" ", seedWords), Wordlist);
		}

		public WalletInfo GetWalletInfo()
		{
			CheckNullEntries();

			Wallet w = new Wallet(ToString(), "");
			return new WalletInfo
			{
				Seedphrase = ToString(),
				Address = w.GetAccount(0).Address
			};
		}

		public Seedphrase Copy()
        {
			Seedphrase retVal = new(seedWords.Length, Wordlist);
			for (int i = 1; i <= 12; ++i)
            {
				retVal[i] = seedWords[i - 1];
            }

			return retVal;
        }

        public override bool Equals(object? obj)
        {
            if (obj is Seedphrase rValue &&
				Wordlist == rValue.Wordlist &&
				seedWords.Length == rValue.seedWords.Length)
            {
				for (int i = 0; i < seedWords.Length; ++i)
                {
					if (seedWords[i] != rValue.seedWords[i])
						return false;
                }

				return true;
            }

			return false;
        }

        public override string ToString()
		{
			StringBuilder strBuilder = new StringBuilder();

			for (int i = 0; i < seedWords.Length; ++i)
			{
				if (seedWords[i] == null)
					strBuilder.Append("[null]");
				else
					strBuilder.Append(seedWords[i]);

				if (i != seedWords.Length - 1)
					strBuilder.Append(' ');
			}
			return strBuilder.ToString();
		}

		private void CheckNullEntries()
		{
			foreach (var word in seedWords)
			{
				if (word == null)
					throw new InvalidOperationException("Seedphrase is missing words.");
			}
		}

		private void CheckWordCount(int count)
        {
			if (count != 12 && count != 18 && count != 24)
				throw new ArgumentException("Seed phrase must be 12, 18, or 24 words.");
		}

		private void CheckValidWord(string? word)
		{
			if (word != null && !Wordlist.WordExists(word, out _))
			{
				throw new ArgumentException(word + " not in the wordlist.");
			}
		}
	}
}
