﻿namespace SeedphraseCracker.Utilities
{
    public class WalletInfo
    {
        public string? Seedphrase { get; set; }

        public string? Address { get; set; }

        public int TxCount { get; set; } = 0;

        public override string ToString()
        {
            return "Seedphrase: " + Seedphrase +
                   "\nAddress: " + Address + 
                   "\nToken Tx Count: " + TxCount;

        }
    }
}