﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SeedphraseCracker.BscScan;
using SeedphraseCracker.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeedphraseCracker.BscScan.Tests
{
    [TestClass]
    public class BscScanApiTests
    {
        [TestMethod]
        public void BscScanApiTest()
        {
            Assert.Fail();
        }

        [TestMethod]
        public async Task CallTest()
        {
            //Valid seedphrase test
            Seedphrase seedphrase = new("[valid_seedphrase_here]");
            var config = Config.Get();
            BscScanApi api = new(config);
            BscScanQuery query = new(config);
            query.Address = seedphrase.GetWalletInfo().Address;

            var result = await api.Call(query);
            Console.WriteLine(result?.ToString());

            Assert.Fail();
        }
    }
}