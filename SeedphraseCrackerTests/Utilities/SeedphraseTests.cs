﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NBitcoin;
using SeedphraseCracker.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeedphraseCracker.Utilities.Tests
{
    [TestClass]
    public class SeedphraseTests
    {
        private const string Seed12 = "village jazz hub brick choose round figure dress time accident desert obscure";
        private const string Seed12Japanese = "あいだ いこく うえき えがお おおい かいわ きいろ ぐあい けさき こいぬ さくし したて";
        private const string InvalidSeed12 = "arm bid car dad egg fat gas hat ill jar figure leg";
        private const string invalidWord = "invalid";

        [TestInitialize]
        public void Initialize()
        {

        }

        //Seedphrase()
        [TestMethod]
        public void SeedphraseTest()
        {
            Seedphrase seedphrase = new Seedphrase();

            Assert.AreEqual(12, seedphrase.WordCount);
            Assert.AreEqual(Wordlist.English, seedphrase.Wordlist);
        }

        //Seedphrase(int, Wordlist)
        [TestMethod]
        public void SeedphraseTest1()
        {
            Seedphrase seedphrase12 = new Seedphrase(12);
            Seedphrase seedphrase18Japanese = new Seedphrase(18, Wordlist.Japanese);

            Assert.AreEqual(12, seedphrase12.WordCount);
            Assert.AreEqual(Wordlist.English, seedphrase12.Wordlist);

            Assert.AreEqual(18, seedphrase18Japanese.WordCount);
            Assert.AreEqual(Wordlist.Japanese, seedphrase18Japanese.Wordlist);

            Assert.ThrowsException<ArgumentException>(() => new Seedphrase(13));
        }

        //Seedphrase(string)
        [TestMethod]
        public void SeedphraseTest2()
        {
            Seedphrase seedphrase12 = new Seedphrase(Seed12);
            Seedphrase seedphrase12Japanese = new Seedphrase(Seed12Japanese, Wordlist.Japanese);
            Seedphrase seedphrase18Trim = new Seedphrase(" " + InvalidSeed12 + " arm arm arm arm arm arm ");

            Assert.AreEqual(12, seedphrase12.WordCount);
            Assert.AreEqual("village", seedphrase12[1]);
            Assert.AreEqual("choose", seedphrase12[5]);
            Assert.AreEqual("obscure", seedphrase12[12]);
            Assert.ThrowsException<IndexOutOfRangeException>(() => seedphrase12[0]);
            Assert.ThrowsException<IndexOutOfRangeException>(() => seedphrase12[13]);

            ArgumentException e;
            e = Assert.ThrowsException<ArgumentException>(() => new Seedphrase(Seed12 + " abandon"));
            Assert.AreEqual("Seed phrase must be 12, 18, or 24 words.", e.Message);

            e = Assert.ThrowsException<ArgumentException>(() => new Seedphrase(Seed12.Substring(0, Seed12.Length - 7) + invalidWord));
            Assert.AreEqual(invalidWord + " not in the wordlist.", e.Message);
        }

        [TestMethod]
        public void TryAddTest()
        {
            Seedphrase seedphrase = new Seedphrase(InvalidSeed12);
            seedphrase[4] = null;
            seedphrase[6] = null;
            seedphrase[8] = null;

            int index;

            var e = Assert.ThrowsException<ArgumentException>(() => seedphrase.TryAdd("invalid", out _));
            Assert.AreEqual(invalidWord + " not in the wordlist.", e.Message);

            Assert.IsTrue(seedphrase.TryAdd("job", out index));
            Assert.AreEqual("job", seedphrase[4]);
            Assert.AreEqual(4, index);
            Assert.IsTrue(seedphrase.TryAdd("loan", out index));
            Assert.AreEqual("loan", seedphrase[6]);
            Assert.AreEqual(6, index);
            Assert.IsTrue(seedphrase.TryAdd("pet", out index));
            Assert.AreEqual("pet", seedphrase[8]);
            Assert.AreEqual(8, index);

            Assert.IsFalse(seedphrase.TryAdd("sea", out _));
        }

        [TestMethod]
        public void GetNullIndicesTest()
        {
            Seedphrase seedphrase = new Seedphrase(InvalidSeed12);

            int[] indices = seedphrase.GetNullIndices();
            Assert.AreEqual(0, indices.Length);

            seedphrase[4] = null;
            seedphrase[6] = null;
            seedphrase[8] = null;

            indices = seedphrase.GetNullIndices();
            Assert.AreEqual(3, indices.Length);
            Assert.AreEqual(4, indices[0]);
            Assert.AreEqual(6, indices[1]);
            Assert.AreEqual(8, indices[2]);
        }

        [TestMethod]
        public void IsValidChecksumTest()
        {
            Seedphrase validPhrase = new(Seed12);
            Seedphrase invalidPhrase = new(InvalidSeed12);
            Seedphrase nullPhrase = new(Seed12);
            nullPhrase[4] = null;

            Assert.IsTrue(validPhrase.IsValidChecksum());
            Assert.IsFalse(invalidPhrase.IsValidChecksum());
            
            var e = Assert.ThrowsException<InvalidOperationException>(() => nullPhrase.IsValidChecksum());
            Assert.AreEqual("Seedphrase is missing words.", e.Message);
        }

        [TestMethod]
        public void ToMnemonicTest()
        {
            Seedphrase seedphrase = new(Seed12);
            Seedphrase nullPhrase = new(Seed12);
            nullPhrase[4] = null;

            var mnemonic = seedphrase.ToMnemonic();
            Assert.IsNotNull(mnemonic);
            
            var e = Assert.ThrowsException<InvalidOperationException>(() => nullPhrase.ToMnemonic());
            Assert.AreEqual("Seedphrase is missing words.", e.Message);
        }

        [TestMethod]
        public void GetWalletInfoTest()
        {
            Seedphrase seedphrase = new(Seed12);
            Seedphrase nullPhrase = new(Seed12);
            nullPhrase[4] = null;

            var walletInfo = seedphrase.GetWalletInfo();
            Assert.IsNotNull(walletInfo);
            Assert.AreEqual(Seed12, walletInfo.Seedphrase);
            Assert.AreEqual("0x7d0264Cfa90d45E353e9F080Fb187eFe5FE5d803", walletInfo.Address);
            Assert.AreEqual(0, walletInfo.TxCount);

            var e = Assert.ThrowsException<InvalidOperationException>(() => nullPhrase.GetWalletInfo());
            Assert.AreEqual("Seedphrase is missing words.", e.Message);
        }

        [TestMethod]
        public void CopyTest()
        {
            Seedphrase source = new(Seed12);
            Seedphrase nullPhrase = new(Seed12);
            nullPhrase[4] = null;

            var copy = source.Copy();
            Assert.IsNotNull(copy);
            Assert.AreEqual(source.Wordlist, copy.Wordlist);
            Assert.AreEqual(source.WordCount, copy.WordCount);

            for (int i = 1; i <= source.WordCount; ++i)
            {
                Assert.AreEqual(source[i], copy[i]);
            }

            var e = Assert.ThrowsException<InvalidOperationException>(() => nullPhrase.GetWalletInfo());
            Assert.AreEqual("Seedphrase is missing words.", e.Message);
        }

        [TestMethod]
        public void EqualsTest()
        {
            Seedphrase seedphrase1 = new(Seed12);
            Seedphrase seedphrase2 = new(Seed12);
            Seedphrase seedphrase3 = new(Seed12);
            seedphrase3[4] = "leg";
            Seedphrase seedphrase4 = new(Seed12Japanese, Wordlist.Japanese);
            Seedphrase nullPhrase = new(Seed12);
            nullPhrase[4] = null;

            Assert.IsTrue(seedphrase1.Equals(seedphrase2));
            Assert.IsTrue(seedphrase2.Equals(seedphrase1));
            Assert.IsFalse(seedphrase1.Equals(seedphrase3));
            Assert.IsFalse(seedphrase3.Equals(seedphrase1));
            Assert.IsFalse(seedphrase1.Equals(seedphrase4));
            Assert.IsFalse(seedphrase4.Equals(seedphrase1));
            Assert.IsFalse(seedphrase1.Equals(null));
        }

        [TestMethod]
        public void ToStringTest()
        {
            Seedphrase seedphrase = new(Seed12);
            Seedphrase nullPhrase = new(Seed12);
            nullPhrase[4] = null;
            nullPhrase[6] = null;

            Assert.AreEqual(Seed12, seedphrase.ToString());
            Assert.AreEqual("village jazz hub [null] choose [null] figure dress time accident desert obscure",
                            nullPhrase.ToString());
        }
    }
}